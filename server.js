var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'text_editor')));

app.get('*', function (req, res) {
    res.sendfile('./text_editor/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.listen(8010);
console.log("App listening on port 8080");
